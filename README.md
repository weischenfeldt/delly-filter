# Delly Filter

Python scripts to work with Delly result files:

> NB: Different delly analyses output (INV, DEL, DUP, INS, TRA)
> need to be merged (eg use BCF/VCF tools)

### Tasks performed by scripts here ###

* Filter the results
* Convert delly VCF/BCF file in bedbpe format
* Annotate the bedpe file with closest genes

### Environment module ###

The tool is installed as a environment module in computerome.
To use the script installed in the module type the command:

```

module add tools bedtools/2.26.0
module add delly-filter

```

The environment module can handle various version of the same
tool.

The path of the modulefiles are installed:

```
/home/projects/cu_10027/apps/modulefiles/delly-filter
```

The path of the python installation (with required dependencies)
and the scripts:

```
/home/projects/cu_10027/apps/software/delly-filter
```


### Pipeline in computerome ###

The `delly-filter` environment module is also included into the
`pype` command in computerome. The snippet wrap the standard
procedure to analyse vcf/bcf results from delly. It takes care
of merge, filter, annotate, index and compress the results.

In order to use the delly-filter in the `pype` command there is
no need to previously load the `delly-filter` module:

```
module add pype
```

To use the tool:

```
pype snippets delly_filter
```