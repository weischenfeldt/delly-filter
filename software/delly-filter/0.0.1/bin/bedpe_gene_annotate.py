#! /usr/bin/env python
"""
###############################
joachim.weischenfeldt@gmail.com
###############################

bedpe_gene_annotate.py
Adds gene annotation.

Usage:
    bedpe_gene_annotate.py -v <BEDPE> [-g <genes>]  [-m <max_distance>] [-x bedfiles (space-separated)]
    genes fuke should be chrom, start, end, gene_name, strand
    bedfiles should be chrom, start, end, name
"""

from __future__ import print_function
import argparse
import sys
import csv
import re
import gzip
import pandas as pd
import numpy as np
import pybedtools
from pybedtools import BedTool
import string
parser = argparse.ArgumentParser('''Adds gene annotation to SV breakpoints''')
parser.add_argument('-v', '--BEDPE', type=str, required=True, help='BEDPE SV file')
parser.add_argument('-g', '--genes', type=str, required=False, help='gene annotation in BED format (chr, start, end, name, strand). Will use gencode v19 without "chr" (optional, default gcode v19)')
parser.add_argument('-m', '--max_dist', type=int, required=False, help='max combined distance to consider between two breakpoints and a gene element (optional)')
parser.add_argument('-x','--bedfiles', type=argparse.FileType('r'), nargs='+', required=False, help = 'add multiple bed files for intersecting (optional)')
parser.set_defaults(genes="/home/projects/cu_10027/data/gene_annotation/gencode/gencode.v19.protein_coding_gene.minimal.bed", max_dist = 50000)
args = parser.parse_args()
bedpe_file = args.BEDPE
gencode_file = args.genes
max_dist = args.max_dist
bedfiles = args.bedfiles
fileOutAnno = re.sub('\.[^.]*$','', bedpe_file) + '.genes.txt'

if float((pybedtools.__version__).split("0.")[1]) < 0.7:
    print ("pybedtools version 0.7 or higher is needed")
    sys.exit(0)

bedpe_header = ["chrom1","start1",  "end1",  "chrom2", "start2",  "end2", "id",  "pairs", "strand1", "strand2", "svtype", "svmethod","scna", "samples"]
header2add = ['chrom_gene1', 'start_gene1', 'end_gene1', 'name_gene1', 'strand_gene1', 'dist_gene1', 'chrom_gene2', 'start_gene2', 'end_gene2', 'name_gene2', 'strand_gene2', 'dist_gene2', 'fusion_orient', 'fusion']
bed_header = ["chrom", "start", "end", "id", "score", "strand"]

def annotate(df_bedpe, annot_bed, max_distance = 50000, event_type = "gene"):
    df_bedpe[["chrom1", "chrom2"]] = df_bedpe[["chrom1", "chrom2"]].astype(str)
    # split BEDPE file into BED file
    df_bed_tmp1 = df_bedpe.iloc[:,[0,1,2,6,7,8]]
    df_bed_tmp1.columns = bed_header
    df_bed_tmp2 = df_bedpe.iloc[:,[3,4,5,6,7,9]]
    df_bed_tmp2.columns = bed_header
    df_bed = df_bed_tmp1.append(df_bed_tmp2)
    df_bed_plusstrand = df_bed[df_bed.strand == "+"]
    df_bed_minusstrand = df_bed[df_bed.strand == "-"]
    plus_bed = BedTool.from_dataframe(df_bed_plusstrand)
    minus_bed = BedTool.from_dataframe(df_bed_minusstrand)
    plus_bed.sort().closest(annot_bed.sort(), D="ref").saveas("sv_gene_plus.bed")
    minus_bed.sort().closest(annot_bed.sort(), D="ref").saveas("sv_gene_minus.bed")
    df_sv_gene_plus = pd.read_csv("sv_gene_plus.bed", header = None, sep ="\t")
    df_sv_gene_minus = pd.read_csv("sv_gene_minus.bed", header = None, sep = "\t")
    df_sv_gene_bed = df_sv_gene_plus.append(df_sv_gene_minus).sort_values([0,1,2])
    if event_type == "gene":
        df_sv_gene_bed.columns = bed_header + ['chrom_' + event_type + '1', 'start_' + event_type + '1', 'end_' + event_type + '1', 'name_' + event_type + '1', 'strand_' + event_type + '1', 'dist_' + event_type + '1']
    elif df_sv_gene_bed.shape[1] == 11:
        df_sv_gene_bed.columns = bed_header + ['chrom_' + event_type + '1', 'start_' + event_type + '1', 'end_' + event_type + '1', 'name_' + event_type + '1', 'dist_' + event_type + '1']
    df_sv_gene_bed["chrom"] = df_sv_gene_bed["chrom"].astype(str)
    df_merge_tmp1 = pd.merge(df_bedpe, df_sv_gene_bed, left_on = ["chrom1", "start1", "id"], right_on = ["chrom", "start", "id"], how = "left")
    df_merge_tmp1.drop(["chrom", "start", "end", "score", "strand"], axis = 1, inplace = True)
    if event_type == "gene":
        df_sv_gene_bed.columns = bed_header + ['chrom_' + event_type + '2', 'start_' + event_type + '2', 'end_' + event_type + '2', 'name_' + event_type + '2', 'strand_' + event_type + '2', 'dist_' + event_type + '2']
    elif df_sv_gene_bed.shape[1] == 11:
        df_sv_gene_bed.columns = bed_header + ['chrom_' + event_type + '2', 'start_' + event_type + '2', 'end_' + event_type + '2', 'name_' + event_type + '2', 'dist_' + event_type + '2']
    df_merge_tmp2 = pd.merge(df_merge_tmp1, df_sv_gene_bed, left_on = ["chrom2", "start2", "id"], right_on = ["chrom", "start", "id"], how = "left")
    df_merge_tmp2.drop(["chrom", "start", "end", "score", "strand"], axis = 1, inplace = True)
    df_bedpe_anno = df_merge_tmp2.copy()
    if event_type == "gene":
        df_bedpe_anno["fusion_orient"] = np.where((df_bedpe_anno["dist_gene1"] + df_bedpe_anno["dist_gene2"] < max_distance) & \
        (df_bedpe_anno["name_gene1"] != df_bedpe_anno["name_gene2"]) & (~df_bedpe_anno["name_gene1"].isnull()) & (~df_bedpe_anno["name_gene2"].isnull()), 
        df_bedpe_anno["strand1"]+df_bedpe_anno["strand2"]+df_bedpe_anno["strand_gene1"]+df_bedpe_anno["strand_gene2"], np.nan)
        df_bedpe_anno["fusion"] = np.where((~df_bedpe_anno["fusion_orient"].isnull()) & \
        ((df_bedpe_anno["fusion_orient"] == "+++-") | \
        (df_bedpe_anno["fusion_orient"] == "+-++") | \
        (df_bedpe_anno["fusion_orient"] == "-+--") | \
        (df_bedpe_anno["fusion_orient"] == "---+") ) \
        , df_bedpe_anno["name_gene1"] + ":" + df_bedpe_anno["name_gene2"], ".")
        df_bedpe_anno["fusion"] = np.where((~df_bedpe_anno["fusion_orient"].isnull()) & \
        ((df_bedpe_anno["fusion_orient"] == "++-+") | \
        (df_bedpe_anno["fusion_orient"] == "+---") | \
        (df_bedpe_anno["fusion_orient"] == "-+++") | \
        (df_bedpe_anno["fusion_orient"] == "--+-") )\
        , df_bedpe_anno["name_gene2"] + ":" + df_bedpe_anno["name_gene1"], ".")
    df_bedpe_anno.fillna(".", inplace = True)
    return (df_bedpe_anno)

df_bedpe = pd.read_csv(bedpe_file, sep = "\t", header = None)
if not  isinstance(df_bedpe.iloc[0, 1], np.int64):
    df_bedpe = pd.read_csv(bedpe_file, sep = "\t", header = None, skiprows = 1)
    df_bedpe.columns = bedpe_header + list(np.arange(len(bedpe_header),df_bedpe.columns.shape[0]))

chrom_w_chr = False
if "chr" in str(df_bedpe.iloc[0,0]):
    chrom_w_chr = True
list_of_beds = list()
if bedfiles:
    for fx in bedfiles:
        list_of_beds.append( BedTool(fx).saveas())

gencode_bed = BedTool(gencode_file)
gencode_chroms = list(set(map(str, gencode_bed.to_dataframe().iloc[:,0].tolist())))
if df_bedpe.empty:
    print ("\nno entries in BEDPE file.", fileOutAnno, "will be empty")
    df_out = pd.DataFrame(columns=df_bedpe.columns.tolist() + header2add)
    df_out.to_csv(fileOutAnno, sep = "\t", index = False)
    sys.exit(0)
if not str(df_bedpe.iloc[0, 0])  in  gencode_chroms:
    print ("chromosome annotation seem to differ between", gencode_file, "and", bedpe_file)
df_out = annotate(df_bedpe, gencode_bed, max_distance = max_dist)
if len(list_of_beds)> 0:
    n = 0
    letters = string.ascii_uppercase
    for x_bed in list_of_beds:
        if (x_bed.field_count() != 4):
            print ("need exactly 4 cols: chrom, start, end, name. Skipping", x_bed.fn, x_bed.head(2))
            continue
        if ("chr" in str(x_bed[0][0]) and not chrom_w_chr):
            print ("both files need to have same chromosome naming scheme", x_bed.head(2))
            continue
        df_out = annotate(df_out, x_bed, max_distance = max_dist, event_type = "region" + letters[n])
        n += 1
df_out.drop_duplicates(inplace = True)
df_out.sort_values(["chrom1", "start1", "chrom2", "start2"], inplace = True)
df_out.to_csv(fileOutAnno, sep = "\t", index = False, na_rep = ".")
print ("\nAnnotation file\n\t", fileOutAnno)
