#! /usr/bin/env python
# DellySomaticFreqFilter.py
from __future__ import print_function
import argparse, numpy, re, collections, copy, vcf, sys, gzip,os
from collections import defaultdict


def overlapValid((s1, e1), (s2, e2), reciprocalOverlap=0.8, maxOffset=250):
    bpOffset = max(abs(s2-s1), abs(e2-e1))
    overlapLen = float(min(e1, e2) - max(s1, s2))
    lenA = float(e1-s1)
    lenB = float(e2-s2)
    # Check for any overlap
    if (e1 < s2) or (s1 > e2) or (lenA <= 0) or (lenB <= 0) or (overlapLen <= 0):
        return False
    # Check reciprocal overlap and offset
    if ((overlapLen/max(lenA, lenB)) < reciprocalOverlap) or (bpOffset > maxOffset):
        return False
    return True


def germSpecFilter(vcfInfo, tumorType, tumorTypeCtrlFrq,tumorTypeTumFrq,max1kGP=0.05):
    germOnly=False
    scList = [sc for sc in vcfInfo if 'SC' in sc]
    sampleSet = set([re.sub('SC_(([^_]*))_[a-z]*_[CF]','\\1',k) for k in scList if 'tumor' in k or 'control' in k])
    if (vcfInfo['SC_1KGP_F'] <= max1kGP \
    or (vcfInfo['SC_1KGP_F'] <= max1kGP*2 \
    and vcfInfo['SC_1KGP_C']<=2)) \
    and vcfInfo.get(tumorTypeCtrlFrq) >0 \
    and vcfInfo.get(tumorTypeCtrlFrq) >= vcfInfo.get(tumorTypeTumFrq):
        germOnly=True
        for k in sampleSet:
            s = [i for i in scList if k in i and i.endswith('F')]
            if len(s) > 1 and vcfInfo.get(s[0]) > vcfInfo.get(s[1]):
                germOnly=False
                break
    return germOnly

def tumSpecFilter(vcfInfo, tumorType, tumorTypeCtrlFrq,tumorTypeTumFrq, maxCtrl=0.05, max1kGP=0.05):
    tumOnly=False
    scList = [sc for sc in vcfInfo if 'SC' in sc]
    if ( float(vcfInfo['SC_1KGP_F']) <= max1kGP \
    or ( float(vcfInfo['SC_1KGP_F']) <= max1kGP*2 \
    and int(vcfInfo['SC_1KGP_C'] )<= 5)) \
    and float(vcfInfo[tumorTypeTumFrq]) > 0:
        for k in scList:
            try:
                varCount = ''.join(map(str,vcfInfo.get(k)))
            except Exception, e:
                varCount = vcfInfo.get(k)
            if k.endswith('control_F') and float(varCount) > maxCtrl:
                tumOnly=False
                break
            else:
                tumOnly=True
    return tumOnly


def RefGeno(call, rcRef):
    if (call.gt_type == 0):
        if ((not precise) and (call['DV'] == 0)) or ((precise) and (call['RV']==0)):
            rcRef.append(call['RC'])
    return rcRef


def AltGeno(call, rcAlt):
    '''
    return RC number (high-quality read counts for the SV) if ALT is confident
    '''
    if (call.gt_type != 0):
        if ((not precise) and (call['DV'] >= suppPairs) \
        and (float(call['DV'])/float(call['DV']+call['DR'])>=altAF)) \
        or ((precise) and (call['RV'] >= suppPairs) \
        and (float(call['RV'])/float(call['RR'] + call['RV'])>=altAF)):
            rcAlt.append(call['RC'])
    return rcAlt




# Parse command line
parser = argparse.ArgumentParser(description='Filter for somatic SVs.')
parser.add_argument('-v', '--vcf', metavar='variants.vcf', required=True, dest='vcfFile', help='input vcf file (required)')
#parser.add_argument('-t', '--type', metavar='DEL', required=True, dest='svType', help='SV type [DEL, DUP, INV, INS] (required)')
parser.add_argument('-o', '--out', metavar='out.vcf', required=False, dest='outFile', help='output vcf file. Default suffix ".highConf.vcf" (optional)')
parser.add_argument('-s', '--sample', metavar='PRAD', required=False, dest='tumorType', help='tumor sample type [PRAD, BLCA, MB...] (default PRAD)')
parser.add_argument('-a', '--altaf', metavar='0.25', required=False, dest='altAF', help='min. alt. AF (default 0)')
parser.add_argument('-m', '--minsize', metavar='100', required=False, dest='minSize', help='min. size (default 100)')
parser.add_argument('-n', '--maxsize', metavar='500000000', required=False, dest='maxSize', help='max. size (optional)')
parser.add_argument('-p', '--tumorpairs', metavar='2', required=False, dest='suppPairs', help='min number of supporing pairs in tumor for high confident filter (default 4)')
parser.add_argument('-r', '--ratioGeno', metavar='0.75', required=False, dest='ratioGeno', help='min. fraction of genotyped samples (optional)')
parser.add_argument('-f', '--filter', dest='siteFilter', action='store_true', help='Filter sites for PASS')
args = parser.parse_args()

# Command-line args
max1kGP=0.01
maxCtrl=0
minSize = 300 ## change 
if args.minSize:
    minSize = int(args.minSize)
maxSize = 500000000
if args.maxSize:
    maxSize = int(args.maxSize)
altAF = 0
if args.altAF:
    altAF = float(args.altAF)
suppPairsHiQ = 4
if args.suppPairs:
    suppPairsHiQ = int(args.suppPairs)
tumorType = ''
if args.tumorType:
    tumorType = args.tumorType
ratioGeno = 0.75
if args.ratioGeno:
    ratioGeno = float(args.ratioGeno)
traWindow = 2500  # 2.5kb translocation window

vcfFile = args.vcfFile
siteFilter = ''
siteFilter = args.siteFilter
#svType = args.svType
outFile = vcfFile.split('.vcf')[0] + '.somatic.vcf'
if args.outFile:
    outFile = args.outFile
outFileSomaticConf = outFile.split('.vcf')[0] + '.highConf.vcf'
outFileGerm = outFile.replace('.somatic', '').split('.vcf')[0] + '.germline.vcf'
outFileGermConf = outFile.replace('.somatic', '').split('.vcf')[0] + '.germline.highConf.vcf'


fileErr = vcfFile.split(".vcf")[-2] + "_annotate_crash.txt"
if os.path.exists(fileErr):
    os.remove(fileErr)

# Collect high-quality SVs
sv = dict()
rdRat = dict()
svDups = defaultdict(list)
validRecordID = set()
validGermRecordID = set()
if vcfFile:
    vcf_reader = vcf.Reader(open(vcfFile), 'r', compressed=True) if vcfFile.endswith('.gz') else vcf.Reader(open(vcfFile), 'r', compressed=False)
    for record in vcf_reader:
        vcfInfo = record.INFO
        chromPair = tuple(sorted([record.CHROM, record.INFO['CHR2']]))
        suppPairs = 2
        sv_len = 0
        if record.CHROM == record.INFO['CHR2']:
            sv_len = abs(int(record.POS) - int(record.INFO['END']))
        #svType = re.sub(r'[0-9]*','',record.ID) 
        svType = record.INFO['SVTYPE']
        popFilter = False
        if tumorType:
            listOfSamples = ', '.join(map(str, list(set([re.sub('SC_((.*))_[a-z]*_[CF]', '\\1', sc) for sc in record.INFO if 'SC_' in sc and '1KGP' not in sc]))))
            if [sc for sc in record.INFO if tumorType in sc]:
                tumorTypeCtrlFrq = 'SC_' + tumorType + '_control_F'
                tumorTypeCtrlCount = 'SC_' + tumorType + '_control_C'
                tumorTypeTumFrq = 'SC_' + tumorType + '_tumor_F'
                tumorTypeTumCount = 'SC_' + tumorType + '_tumor_C'
                # must be present min once in tumor or germline of self
                if record.INFO[tumorTypeCtrlCount] > 0 or record.INFO[tumorTypeTumCount] > 0:
                    popFilter = True
                elif (listOfSamples and tumorType):
                    if not tumorType in listOfSamples:
                        errorLine = tumorType + " not present in INFO field of {0}. please skip tumorType or use one of these tumor samples [-s]: {1}\n\n\n".format((record.CHROM, record.POS, record.ID), listOfSamples)
                    else:
                        errorLine = 'annotation not present for {0}.\n'.format((record.CHROM, record.POS, record.ID))
                    # print (errorLine)
                    crashfile = open(fileErr, 'a')
                    crashfile.writelines(errorLine)
                    crashfile.close()
                    #sys.exit(-1)
            else:
                popFilter = False
        if ((sv_len >= minSize) and (sv_len <= maxSize) or (svType == 'TRA'))  and ((not siteFilter) or (len(record.FILTER) == 0)):
            precise = False
            if ('PRECISE' in record.INFO.keys()):
                precise = record.INFO['PRECISE']
            rcRef = []
            rcAlt = []
            rcGermAlt = []
            genoRef = set()
            genoAlt = set()
            prercAlt = []
            callTumRV = 0
            callTumDV = 0
            rdRatio = 1
            isTumSpec = False
            isGermSpec = False
            nCount = 0
            tCount = 0
            n_samples = len(record.samples) -1
            for e, call in enumerate(record.samples):
                if (call.called):
                    if (re.search(r"[Bb]lood|[Cc]ontrol|[Gg]erm|[Nn]ormal|_N0[0-9]|_DNA_B_", call.sample) != None):
                        nCount += 1
                        genoRef.add( call['GT'])
                        rcRef = RefGeno(call, rcRef)
                        rcGermAlt = AltGeno(call, rcGermAlt)
                    elif (re.search(r"[Tt]umor|_DNA_T_|_T0[0-9]|_Tx_", call.sample) != None):
                        tCount += 1
                        lenAlt = len(rcAlt)
                        rcAlt= AltGeno(call, rcAlt)
                        if lenAlt != len(rcAlt):
                            genoAlt.add(call['GT'])
                        callTumRV += call['RV']
                        callTumDV += call['DV']
                    elif len(record.samples) > 1: # If no 'tumor' or 'normal' in samples, assume that the last is normal!!!
                        if (e == 0):
                            tCount += 1
                            lenAlt = len(rcAlt)
                            rcAlt = AltGeno(call, rcAlt)
                            if lenAlt != len(rcAlt):
                                genoAlt.add(call['GT'])
                            callTumRV += call['RV']
                            callTumDV += call['DV']
                        if (e == n_samples):
                            # is last sample
                            nCount += 1
                            genoRef.add(call['GT'])
                            rcRef = RefGeno(call, rcRef)
                            rcGermAlt = AltGeno(call, rcGermAlt) # get germline specific calls
                        else:
                            tCount += 1
                            lenAlt = len(rcAlt)
                            rcAlt= AltGeno(call, rcAlt)
                            if lenAlt != len(rcAlt):
                                genoAlt.add(call['GT'])
                            callTumRV += call['RV']
                            callTumDV += call['DV']
                    elif len(record.samples) == 1: # If only one sample, assume that the sample is tumor
                        genoAlt.add( call['GT'])
                        rcAlt = AltGeno(call, rcAlt)
                        rcRef = rcAlt
                        callTumRV += call['RV']
                        callTumDV += call['DV']
            genotypeRatio = float(nCount + tCount) /  float(len(record.samples))
            if not genotypeRatio >= ratioGeno: # check that ratio of samples are genotyped
                continue
            rdRatio = 1
            if rcAlt and rcRef and numpy.median(rcRef)>0:
                rdRatio = round(numpy.median(rcAlt)/numpy.median(rcRef),4)
                if rdRatio > 2:
                    suppPairs = suppPairs + suppPairs * round(rdRatio/2.0)
            rdRat[record.ID] = rdRatio
            if (len(rcRef) > 0) and (len(rcAlt) > 0) and genoRef != genoAlt:
                isTumSpec = True
            elif (len(rcGermAlt) > 0) and (len(rcAlt) > 0) and genoRef == genoAlt:
                isGermSpec = True
            else:
                continue
            # Filtering: If DEL or DUP: Smaller than 10kb OR RD<0.85 OR RD>1.15 OR supporting pairs +1
            if (isTumSpec or isGermSpec) and ((callTumRV >0 and sv_len >= minSize) or ( sv_len >= 500 or sv_len == 0)) and (callTumRV  + callTumDV >= suppPairs) and ((svType == 'INV') or (svType == 'INS') or (sv_len <= 10000) \
            or (callTumRV + callTumDV >= suppPairs+1) or ((svType == 'DEL') and (rdRatio <= 0.85)) or ((svType == 'DUP') and (rdRatio >= 1.15))):
                if isGermSpec:
                    validGermRecordID.add(record.ID)
                elif isTumSpec:
                    if popFilter:
                        if tumSpecFilter(vcfInfo, tumorType, tumorTypeCtrlFrq,tumorTypeTumFrq, maxCtrl, max1kGP):
                            validRecordID.add(record.ID)
                        else:
                            next
                    else:
                        validRecordID.add(record.ID)
                if not sv.has_key( chromPair ):
                    sv[chromPair] = dict()
                if svType == 'TRA':
                    if (record.POS - traWindow, record.POS + traWindow) in sv[chromPair]:
                        svDups[(record.CHROM, record.POS - traWindow, record.POS + traWindow)].append((record.ID, record.INFO['PE']))
                    else:
                        sv[chromPair][(record.POS - traWindow, record.POS + traWindow)] = (record.ID, record.INFO['PE'])
                    if (record.INFO['END'] - traWindow, record.INFO['END'] + traWindow) in sv[chromPair]:
                        svDups[(record.INFO['CHR2'], record.INFO['END'] - traWindow, record.INFO['END'] + traWindow)].append((record.ID, record.INFO['PE']))
                    else:
                        sv[chromPair][(record.INFO['END'] - traWindow, record.INFO['END'] + traWindow)] = (record.ID, record.INFO['PE'])
                else:
                    if (record.POS, record.INFO['END']) in sv[chromPair]:
                        svDups[(record.CHROM, record.POS, record.INFO['END'])].append((record.ID, record.INFO['PE']))
                    else:
                        sv[chromPair][(record.POS, record.INFO['END'])] = (record.ID, record.INFO['PE'])

# Output vcf records
if vcfFile:
    vcf_reader=vcf.Reader(open(vcfFile), 'r', compressed=True) if vcfFile.endswith('.gz') else vcf.Reader(open(vcfFile), 'r', compressed=False)
    vcf_reader.infos['SOMATIC'] = vcf.parser._Info('SOMATIC', 0, 'Flag', 'Somatic structural variant.', ',',',')
    vcf_reader.infos['GERMLINE'] = vcf.parser._Info('GERMLINE', 0, 'Flag', 'Germline structural variant.', ',',',')
    vcf_reader.infos['RDRATIO'] = vcf.parser._Info('RDRATIO', 1, 'Float', 'Read-depth ratio of tumor vs. normal.', ',',',')
    vcf_writer = vcf.Writer(open(outFile, 'w'), vcf_reader, lineterminator='\n')
    vcfConf_writer = vcf.Writer(open(outFileSomaticConf, 'w'), vcf_reader, lineterminator='\n')
    vcf_Germwriter = vcf.Writer(open(outFileGerm, 'w'), vcf_reader, lineterminator='\n')    
    vcfConf_Germwriter = vcf.Writer(open(outFileGermConf, 'w'), vcf_reader, lineterminator='\n')
    for record in vcf_reader:
        chromPair = tuple(sorted([record.CHROM, record.INFO['CHR2']]))
        svType = record.INFO['SVTYPE']
        # Is it a valid SV?
        if (record.ID not in validGermRecordID and record.ID not in validRecordID):
            continue
        #Collect overlapping and identical calls
        overlapCalls = list()
        for cSvID, cScore in svDups[(chromPair, record.POS, record.INFO['END'])]:
            if (record.ID != cSvID):
                overlapCalls.append((cSvID, cScore))
        startIntersect = list(set( sv[chromPair].keys()).intersection(set([record.POS, record.INFO['END']])))
        if startIntersect:
            for cStart, cEnd in startIntersect[0]:
                cSvID, cScore = sv[chromPair][(cStart, cEnd)]
                if (record.ID != cSvID) and (overlapValid(record.POS, record.INFO['END'], cStart, cEnd)):
                    overlapCalls.append((cSvID, cScore))
         # Judge wether overlapping calls are better
        foundBetterHit = False
        for cSvID, cScore in overlapCalls:
            if (cScore > record.INFO['PE']) or ((cScore == record.INFO['PE']) and (cSvID < record.ID)):
                foundBetterHit = True
                break
        if not foundBetterHit:
            record.INFO['RDRATIO'] = rdRat[record.ID]
            if record.ID in validRecordID:
                record.INFO['SOMATIC'] = True
                vcf_writer.write_record(record)
                if int(record.INFO['MAPQ']) >= 20 and int(record.INFO['PE']>=suppPairsHiQ) and not record.FILTER:
                    vcfConf_writer.write_record(record)
            if record.ID in validGermRecordID:
                record.INFO['GERMLINE'] = True
                vcf_Germwriter.write_record(record)
                if int(record.INFO['MAPQ']) >= 20 and int(record.INFO['PE']>=suppPairsHiQ) and not record.FILTER:
                    vcfConf_Germwriter.write_record(record)


print ("\nGenerated filter files:\n@SOMATIC\n\t{0}\n\t{1}\n@GERMLINE\n\t{2}\n\t{3}".format(outFile,outFileSomaticConf, outFileGerm, outFileGermConf))
