#!/usr/bin/env python2.7

import gzip
import sys
import argparse
import logging
from pysam import VariantFile

def xopen(filename, mode = 'r'):
   '''
   Replacement for the "open" function that can also open
   files that have been compressed with gzip. If the filename ends with .gz,
   the file is opened with gzip.open(). If it doesn't, the regular open()
   is used. If the filename is '-', standard output (mode 'w') or input
   (mode 'r') is returned.
   '''
   assert isinstance(filename, str)
   if filename == '-':
      return sys.stdin if 'r' in mode else sys.stdout
   if filename.endswith('.gz'):
      return gzip.open(filename, mode)
   else:
      return open(filename, mode)

#dump_file = '/home/projects/cu_10027/projects/basespace_test/data/data_processed/36084058/sv/delly/HCC15WGA/HCC15WGA_DEL.pe_dump.txt.gz'
#bcf_file = '/home/projects/cu_10027/projects/basespace_test/data/data_processed/36084058/sv/delly/HCC15WGA/HCC15WGA_DEL.bcf'

def reads_id(dump):
    rn_dict = dict()
    with xopen(dump, 'rt') as pe_dump:
        header = next(pe_dump)
        sv_id = None
        reads_id = []
        for line in pe_dump:
            sv_line = line.strip().split('\t')
            if sv_line[0] != sv_id and sv_id is not None:
                # yield {sv_id: ','.join(reads_id)}
                rn_dict[sv_id] =  ','.join(reads_id)
                sv_id =	sv_line[0]
                reads_id = [sv_line[2]]
            else:
                sv_id = sv_line[0]
                reads_id.append(sv_line[2])
        # yield {sv_id: ','.join(reads_id)}
        rn_dict[sv_id] =  ','.join(reads_id)
    return rn_dict

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--vcf',  dest='vcf', required=True,
                        help='VCF/BCF file to annotate')
    parser.add_argument('-p', '--pe', dest='pe', required=True,
                        help=('File with sv_id and reads_id, '
                              'usually the output of the -p option'
                              ' of delly call command'))
    parser.add_argument('-o', '--out', dest='out', required=True,
                        help='output VCF/BCF file')

    args = parser.parse_args()

    FORMAT = '%(levelname)s : %(asctime)s : %(message)s'
    logging.basicConfig(format=FORMAT)
    logger = logging.getLogger('sv_rn')
    logger.setLevel('INFO')
    if args.out.endswith('.bcf'):
        write_type = 'wb'
    else:
       write_type = 'w'

    with VariantFile(args.vcf) as bcf_in:
        bcf_in.header.info.add('READ_ID', None, 'String',
                               'read names of supporting pairs')
        with VariantFile(args.out, write_type, header=bcf_in.header) as bcf_out:
            sv_reads = reads_id(args.pe)
            for rec in bcf_in.fetch():
                try:
                    rec.info['READ_ID'] = sv_reads[rec.id]
                except KeyError:
                    logger.info('SV %s is not associated with read names' % rec.id)
                bcf_out.write(rec)


if __name__ == '__main__':
   main()
